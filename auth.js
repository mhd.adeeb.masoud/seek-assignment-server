
const { User, Hirer } = require('./database')
const jsonwebtoken = require("jsonwebtoken");

const JWT_SECRET =
    "goK!pusp6ThEdURUtRenOwUhAsWUCMheBazl!uJLPlS8EbreWLdrwpIwabRAsiBu";

const authenticateUserWithemail = (user) => {
    return new Promise((resolve, reject) => {
        try {
            User.findOne({
                where: {
                    user_email: user.email,
                },
                include: Hirer,
            }).then(async (response) => {
                if (!response) {
                    resolve(false);
                } else {
                    if (
                        !response.dataValues.password ||
                        !(await response.validPassword(
                            user.password,
                            response.dataValues.password
                        ))
                    ) {
                        resolve(false);
                    } else {
                        resolve(response.dataValues);
                    }
                }
            });
        } catch (error) {
            const response = {
                status: 500,
                data: {},
                error: {
                    message: "user match failed",
                },
            };
            reject(response);
        }
    });
};

const verifyToken = (req, res, next) => {
    const bearerHeader = req.headers["authorization"];
    if (bearerHeader && typeof bearerHeader != undefined) {
        const bearer = bearerHeader.split(" ");
        const token = bearer[1];
        const decodedHeader = jsonwebtoken.verify(token, JWT_SECRET);
        if (!decodedHeader) {
            res
                .status(401)
                .json({ message: "You need to be logged in to access this resource" });
        }
        req.authHeader = decodedHeader;
        next();
    } else {
        res
            .status(401)
            .json({ message: "You need to be logged in to access this resource" });
    }
};

const login = async (req, res) => {
    const { email, password } = req.body;
    console.log(`${email} is trying to login ..`);

    const result = await authenticateUserWithemail(req.body);
    if (result)
        return res.json({
            token: jsonwebtoken.sign(
                { user_id: result.id, hirer_id: result.hirerId },
                JWT_SECRET
            ),
            user: result.hirer.companyName,
        });

    return res
        .status(401)
        .json({ message: "The email and/or password your provided are invalid" });
}

module.exports = { verifyToken, login }
