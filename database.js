const Sequelize = require("sequelize");
const bcrypt = require("bcrypt");

// const database = new Sequelize({
//   dialect: 'sqlite',
//   storage: './test.sqlite'
// })
const database = new Sequelize("sqlite::memory:");

const JobPost = database.define("jobPost", {
  positionTitle: Sequelize.STRING,
  jobDescription: Sequelize.TEXT,
  postingDate: {
    type: Sequelize.DATE,
    defaultValue: Sequelize.NOW,
  },
  salary: Sequelize.RANGE(Sequelize.INTEGER),
  status: Sequelize.ENUM("closed", "pending", "open"),
});

const Location = database.define("location", {
  name: Sequelize.STRING,
});

const Hirer = database.define("hirer", {
  companyName: Sequelize.STRING,
  companyDescription: Sequelize.TEXT,
});

const User = database.define(
  "user",
  {
    password: {
      field: "user_password",
      type: Sequelize.STRING,
      allowNull: true,
    },
    email: {
      type: Sequelize.STRING,
      field: "user_email",
      unique: true,
      allowNull: false,
    },
  },
  {
    hooks: {
      beforeCreate: async (user) => {
        if (user.password) {
          const salt = await bcrypt.genSaltSync(10, "a");
          user.password = bcrypt.hashSync(user.password, salt);
        }
      },
      beforeUpdate: async (user) => {
        if (user.password) {
          const salt = await bcrypt.genSaltSync(10, "a");
          user.password = bcrypt.hashSync(user.password, salt);
        }
      },
    },
    instanceMethods: {
      validPassword: (password) => {
        return bcrypt.compareSync(password, this.password);
      },
    },
  }
);

User.prototype.validPassword = async (password, hash) => {
  return await bcrypt.compareSync(password, hash);
};

//Associations
Hirer.hasOne(User);
User.belongsTo(Hirer);

Hirer.hasMany(JobPost);
JobPost.belongsTo(Hirer);

Location.belongsToMany(JobPost, { through: "JobPostLocations" });
JobPost.belongsToMany(Location, { through: "JobPostLocations" });

const initializeDatabase = async (app) => {
  await database.sync();
  await fillDatabase();
};

const fillDatabase = async () => {
  const kl = await Location.create({ name: "Kuala Lumpur" });
  const syd = await Location.create({ name: "Sydney" });
  const dam = await Location.create({ name: "Damascus" });
  const bl = await Location.create({ name: "Berlin" });
  const sg = await Location.create({ name: "Singapore" });

  const seek = await Hirer.create({
    companyName: "Seek",
    companyDescription: "very nice company",
  });
  await User.create({
    email: "admin@test.com",
    password: "admin123",
    hirerId: seek.id,
  });

  const android = await JobPost.create({
    positionTitle: "Android Developer",
    jobDescription: "Develop Android Apps",
    salary: [1000, 2000],
    status: "open",
    hirerId: 1,
  });
  await android.setLocations([kl, sg]);
  const ios = await JobPost.create({
    positionTitle: "iOS Developer",
    jobDescription: "Develop iOS Apps",
    salary: [2000, 3000],
    status: "pending",
    hirerId: 1,
  });
  await ios.setLocations([syd, bl, kl]);
  const be = await JobPost.create({
    positionTitle: "Backend Developer",
    jobDescription:
      "Develop Backend system and \ntake care of the aws instances. \nManage the deliveries of the API gateway. \nhandle on-call rotation",
    salary: [3000, 4000],
    status: "closed",
    hirerId: 1,
  });
  await be.setLocations([dam]);

  console.log("db filled");
};

const listJobPosts = async (hirerId) => {
  return JobPost.findAll({
    attributes: ["id", "name"],
    where: {
      hirerId: hirerId,
    },
    attributes: { exclude: ["createdAt", "updatedAt"] },
    include: {
      model: Location,
      through: {
        attributes: [],
      },
    },
    order: [["postingDate", "DESC"]],
  });
};

const insertJobPost = async (jobPost, hirerId) => {
  const newPost = await JobPost.create({
    positionTitle: jobPost.positionTitle,
    jobDescription: jobPost.jobDescription,
    salary: jobPost.salary,
    status: jobPost.status,
    hirerId: hirerId,
  });
  await newPost.setLocations(jobPost.locationIds);
  return newPost;
};

const listLocations = () => {
  return Location.findAll({
    attributes: ["id", "name"],
    order: [["name", "ASC"]],
  });
};

module.exports = {
  initializeDatabase,
  listJobPosts,
  insertJobPost,
  listLocations,
  User,
  Hirer
};
