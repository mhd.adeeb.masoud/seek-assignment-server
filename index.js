const express = require("express");
const bodyParser = require("body-parser");
const { promisify } = require("util");
const { login, verifyToken } = require('./auth')

const {
  initializeDatabase,
  listJobPosts,
  insertJobPost,
  listLocations,
} = require("./database");


const app = express();
app.use(bodyParser.json());

app.get("/jobPosts", verifyToken, async (req, res) => {
  const hirerId = req.authHeader.hirer_id
  if (!hirerId) {
    res.status(401)
      .json({ message: "You need to be logged in to access this resource" });
  } else {
    const posts = await listJobPosts(hirerId);
    console.log(`Posts: ${JSON.stringify(posts)}`);
    res.status(200).json(posts);
  }
});

app.get("/locations", async (req, res) => {
  const locations = await listLocations();
  if (!locations) {
    res.status(200).json([])
  }
  console.log(`locations: ${JSON.stringify(locations)}`);
  res.status(200).json(locations);
});

app.post("/jobPost", verifyToken, async (req, res) => {
  const hirerId = req.authHeader.hirer_id
  if (!hirerId) {
    res.status(401)
      .json({ message: "You need to be logged in to access this resource" });
  } else {
  const post = await insertJobPost(req.body, req.authHeader.hirer_id);
  console.log(`New Post Added: ${JSON.stringify(post)}`);
  res.status(201).json(post);
  }
});

app.post("/login", async (req, res) => {
  return await login(req, res)
});

const startServer = async () => {
  await initializeDatabase(app);
  const port = process.env.SERVER_PORT || 3000;
  await promisify(app.listen).bind(app)(port);
  console.log(`Listening on port ${port}`);
};

startServer();
